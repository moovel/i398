package string;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@SuppressWarnings("unused")
public class OrderService {

    private DataSource dataSource;

    public OrderService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<Order> getFilledOrders() {

        List<Order> filledOrders = new ArrayList<Order>();
        for (Order order : dataSource.getOrders()) {
            if (order.isFilled()) {
                filledOrders.add(order);
            }
        }
        return filledOrders;
    }

    public List<Order> getOrdersOver(double amount) {

        List<Order> ordersOver = new ArrayList<Order>();
        for(Order order : dataSource.getOrders()){
            if(order.getTotal() > amount){
                ordersOver.add(order);
            }
        }

        return ordersOver;
    }

    public List<Order> getOrdersSortedByDate() {
        List<Order> ordersListByDate = new ArrayList<Order>();
        for(Order order : dataSource.getOrders()){
            ordersListByDate.add(order);
        }
        ordersListByDate.sort(Comparator.comparing(o -> o.getOrderDate()));
        return ordersListByDate;
    }
}
