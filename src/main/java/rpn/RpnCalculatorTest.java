package rpn;

import org.junit.jupiter.api.Test;

import java.util.Stack;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class RpnCalculatorTest {

    /*
    assertThat(c.getAccumulator(), is(0));
    c.setAccumulator(1);
    assertThat(c.getAccumulator(), is(1));
    c.enter();
    c.setAccumulator(2);
    c.plus();
    assertThat(c.getAccumulator(), is(3));
*/


    @Test
    public void newCalculatorHasZeroInItsAccumulator() {
        RpnCalculator c = new RpnCalculator();

        assertThat(c.getAccumulator(), is(0));

    }
    @Test
    public void newCalculatorControlEnteredVaule() {
        RpnCalculator c = new RpnCalculator();

        c.setAccumulator(1);
        assertThat(c.getAccumulator(), is(1));
    }

    @Test
    public void newCalculatorSumVaules() {
        RpnCalculator c = new RpnCalculator();

        c.setAccumulator(1);
        c.enter();
        c.setAccumulator(2);
        c.plus();
        assertThat(c.getAccumulator(), is(3));
    }

    @Test
    public void newCalculatorSumAndMultiplyVaules() {
        RpnCalculator c = new RpnCalculator();

        c.setAccumulator(1);
        c.enter();
        c.setAccumulator(2);
        c.plus();
        c.enter();
        c.setAccumulator(4);
        c.multiply();
        assertThat(c.getAccumulator(), is(12));
    }


    @Test
    public void newCalculatorSumsAndMultiplyVaules() {
        RpnCalculator c = new RpnCalculator();

        c.setAccumulator(4);
        c.enter();
        c.setAccumulator(3);
        c.plus();
        c.enter();
        c.enter();
        c.setAccumulator(2);
        c.enter();
        c.setAccumulator(1);
        c.plus();
        c.multiply();
        assertThat(c.getAccumulator(), is(21));
    }

}

class RpnCalculator {

    Stack<Integer> stk  = new Stack();



    int accumulator;

    public Integer getAccumulator() {
        return accumulator;
    }

    public void setAccumulator(int value) {
        this.accumulator = value;
    }

    public void enter() {
        stk.push(accumulator);
    }

    public void plus() {
        accumulator += stk.pop();
    }

    public void minus() {
        accumulator -= stk.pop();
    }

    public void multiply() {
        accumulator *= stk.pop();
    }



}