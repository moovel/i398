package invoice;

import java.math.BigDecimal;
import java.util.Date;

interface InvoiceRowDao {
    void save(InvoiceRow ir);
}