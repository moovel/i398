package invoice;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.mockito.Mockito.*;
import static org.mockito.hamcrest.MockitoHamcrest.argThat;


public class InvoiceRowGeneratorTest {

    @Mock InvoiceRowDao dao;

    @InjectMocks
    InoviceRowGenerator generator;

    @Test
    public void arePeriodDatesDividedCorrectly() {

        BigDecimal amount = new BigDecimal(100);
        LocalDate periodStart = asDate("2017-01-01");
        LocalDate periodEnd = asDate("2017-02-02");

        generator.generateInvoiceRows(amount, periodStart, periodEnd);

        verify(dao, times(1)).save(argThat(getMatcherForDate(asDate("2017-01-01"))));
        verify(dao, times(1)).save(argThat(getMatcherForDate(asDate("2017-02-01"))));

        // check that it produced two rows with dates 2017-01-01 and 2017-02-01
    }


    @Test
    public void periodPaymentAmountsDividesEqually_WhenDivisible() {

        BigDecimal amount = new BigDecimal(9);
        LocalDate periodStart = asDate("2017-01-01");
        LocalDate periodEnd = asDate("2017-03-04");

        generator.generateInvoiceRows(amount, periodStart, periodEnd);
        verify(dao, times(1)).save(argThat(getMatcherForDate(asDate("2017-01-01"))));
        verify(dao, times(1)).save(argThat(getMatcherForDate(asDate("2017-02-01"))));
        verify(dao, times(1)).save(argThat(getMatcherForDate(asDate("2017-03-01"))));
        verify(dao, times(3)).save(argThat(getMatcherForAmount(3)));
    }

    @Test
    public void periodPaymentAmountsAreDividedCorrectly_WhenNotDivisible() {

        BigDecimal amount = new BigDecimal(11);
        LocalDate periodStart = asDate("2017-01-01");
        LocalDate periodEnd = asDate("2017-03-04");

        generator.generateInvoiceRows(amount, periodStart, periodEnd);
        verify(dao, times(1)).save(argThat(getMatcherForDate(asDate("2017-01-01"))));
        verify(dao, times(1)).save(argThat(getMatcherForDate(asDate("2017-02-01"))));
        verify(dao, times(1)).save(argThat(getMatcherForDate(asDate("2017-03-01"))));
        verify(dao, times(1)).save(argThat(getMatcherForAmount(3)));
        verify(dao, times(2)).save(argThat(getMatcherForAmount(4)));
        // check that it produced three rows with amounts 3, 4 and 4
    }

    @Test
    public void periodRemainedPaymentAmountIsSmallerThanThree() {

        BigDecimal amount = new BigDecimal(7);
        LocalDate periodStart = asDate("2017-01-01");
        LocalDate periodEnd = asDate("2017-04-02");

        generator.generateInvoiceRows(amount, periodStart, periodEnd);

        verify(dao, times(1)).save(argThat(getMatcherForAmount(3)));
        verify(dao, times(1)).save(argThat(getMatcherForAmount(4)));
    }

    @Test
    public void testingDaoWithMockitoAndMatcherExample() {
        dao.save(new InvoiceRow(new BigDecimal(1), LocalDate.now()));

        verify(dao).save(argThat(getMatcherForAmount(1)));
    }

    @Test
    public void periodPaymentAmountIsLessThanThree() {

        BigDecimal amount = new BigDecimal(2);
        LocalDate periodStart = asDate("2017-01-01");
        LocalDate periodEnd = asDate("2017-03-02");

        generator.generateInvoiceRows(amount, periodStart, periodEnd);

        verify(dao, times(1)).save(argThat(getMatcherForDate(asDate("2017-01-01"))));
        verify(dao, times(1)).save(argThat(getMatcherForAmount(2)));

    }

    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    private Matcher<InvoiceRow> getMatcherForAmount(final Integer amount) {
        // Example matcher for testing that argument has certain amount.

        return new TypeSafeMatcher<InvoiceRow>() {

            @Override
            protected boolean matchesSafely(InvoiceRow item) {
                return amount.equals(item.amount.intValue());
            }

            @Override
            public void describeTo(Description description) {
                String message = MessageFormat.format(
                        "InvoiceRow with amount {0}", amount);

                description.appendText(message);
            }
        };
    }

    private Matcher<InvoiceRow> getMatcherForDate(final LocalDate date) {

        return new TypeSafeMatcher<InvoiceRow>() {

            @Override
            protected boolean matchesSafely(InvoiceRow item) {
                return date.equals(item.getDate());
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(String.valueOf(date));
            }
        };
    }

    private LocalDate asDate(String string) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return LocalDate.parse(string, formatter);
    }

}