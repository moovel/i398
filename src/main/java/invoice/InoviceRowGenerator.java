package invoice;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public class InoviceRowGenerator {

    @SuppressWarnings("unused")
    private InvoiceRowDao invoiceRowDao;

    public void generateInvoiceRows(BigDecimal amount, LocalDate start, LocalDate end) {

        List<InvoiceRow> invoiceRows= new ArrayList<>();
        List<LocalDate> dates = getDates(start, end);

        int divideCounter= dates.size();

        BigDecimal remainingAmount = amount;

        for (int i = 0; i < dates.size(); i++) {
            BigDecimal substracted = remainingAmount.subtract(new BigDecimal(String.valueOf(3)));

            if(substracted.compareTo(new BigDecimal(String.valueOf(3))) < 0){
                InvoiceRow row = new InvoiceRow(remainingAmount, dates.get(i));
                invoiceRows.add(row);
                break;
            }else{
                int payment = calculateAmount(remainingAmount, divideCounter);
                InvoiceRow row = new InvoiceRow(new BigDecimal(String.valueOf(payment)), dates.get(i));
                invoiceRows.add(row);

                remainingAmount = remainingAmount.subtract(new BigDecimal(String.valueOf(payment)));
                divideCounter--;
            }

        }
        insertInvoiceRow(invoiceRows);

    }

    public List<LocalDate> getDates(LocalDate start, LocalDate end){

        ArrayList<LocalDate> dates= new ArrayList<>();
        LocalDate date= start;

        while (date.isBefore(end)) {
            dates.add(date);
            date= NextMonthsDate(date);

        }
        return dates;
    }

    private LocalDate NextMonthsDate(LocalDate date) {
        return  date.plusMonths(1).withDayOfMonth(1);
    }

    private int calculateAmount(BigDecimal amount, int divideCounter){

        int calculatedAmount = amount.divide(new BigDecimal(String.valueOf(divideCounter)), 2, RoundingMode.HALF_UP).intValue();
        if (calculatedAmount<3){
            return 3;
        }
        return calculatedAmount;
    }

    private void insertInvoiceRow(List<InvoiceRow> invoiceRows) {
        for (InvoiceRow row : invoiceRows) {
            invoiceRowDao.save(row);
        }
    }

}