package stringcalc;

import org.junit.Test;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class StringCalculatorTest {

    // "" -> 0
    // "1" -> 1
    // "1, 2" -> 3
    // null -> IllegalArgumentException

    @Test
    public void emptyString_returnsZero() {
         StringCalculator c = new StringCalculator();

         int result = c.add("");

         assertThat(result, is(0));
    }
    @Test
    public void oneValue_returnsValue() {
         StringCalculator c = new StringCalculator();

         int result = c.add("1");

         assertThat(result, is(1));
    }

    @Test
    public void twoValues_returnsSumOfValues() {
         StringCalculator c = new StringCalculator();

         int result = c.add("1, 2");

         assertThat(result, is(3));
    }

    @Test (expected = IllegalArgumentException.class)
    public void noValues_returnsException() {
         StringCalculator c = new StringCalculator();

         int result = c.add(null);

    }

}
