package stack;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class StackTest {

    @Test
    public void newStackHasNoElements() {
        Stack stack = new Stack(100);

        assertThat(stack.getSize(), is(0));
    }

    @Test
    public void newStackOneElement() {
        Stack stack = new Stack(100);
        stack.push(1);
        stack.push(2);
        assertThat(stack.getSize(), is(2));
    }
    @Test
    public void newStackPushAndPopEqualsNull() {
        Stack stack = new Stack(100);
        stack.push(1);
        stack.push(1);
        stack.pop();
        stack.pop();
        assertThat(stack.getSize(), is(0));
    }
    @Test
    public void newStackPushAndPopSameElements() {
        Stack stack = new Stack(100);
        stack.push(1);
        stack.push(2);
        assertThat(stack.pop(), is(2));
        assertThat(stack.pop(), is(1));

    }

    @Test
    public void newStackPushAndControllLastElement() {
        Stack stack = new Stack(100);
        stack.push(1);
        stack.push(2);
        assertThat(stack.peek(), is(2));
    }

    @Test
    public void newStackPushTwoAndControllLastElementAndSize() {
        Stack stack = new Stack(100);
        stack.push(1);
        stack.push(2);
        assertThat(stack.peek(), is(2));
        assertThat(stack.getSize(), is(2));
    }

    @Test
    public void newStackPushTwoAndControllPeekFuntsionality() {
        Stack stack = new Stack(100);
        stack.push(1);
        stack.push(2);
        Integer peek1 = stack.peek();
        Integer peek2 = stack.peek();
        assertThat(peek1, is(peek2));
    }

    @Test (expected = IllegalStateException.class)
    public void newStackPopException() {
        Stack stack = new Stack(100);
        stack.pop();
    }
    @Test (expected = IllegalStateException.class)
    public void newStackPeekException() {
        Stack stack = new Stack(100);
        stack.peek();
    }
}