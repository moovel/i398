package gol;


import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;

@SuppressWarnings("unused")
public class GameOfLifeTest {
    Frame frame = new Frame(6, 5);
    // The good place to start is to be able to mark cells as alive.
    @Test
    public void frameSetAliveCells() {
        this.frame.markAlive(0,0);
        this.frame.markAlive(0,0);
        this.frame.markAlive(0,0);
        System.out.println(frame.toString());
        assertThat(frame.isAlive(0,0), is(true));
    }

    // Then it is possible to count alive neighbors.
    @Test
    public void countAliveNeighbors() {
        frame.markAlive(0,0);
        frame.markAlive(0,1);
        frame.markAlive(1,0);
        frame.markAlive(2,0);
        frame.markAlive(0,2);
        assertThat(frame.isAlive(1,1), is(false));
        System.out.println(frame.toString());
        assertThat(frame.getNeighbourCount(1,1), anyOf(is(2), is(5)));
    }


    //Kui rakul on alla kahe naabri, siis ta sureb (alarahvastatus). Alumine piirjuht.
    @Test
    public void cellWithUnderTwoNeighbors_dies() {

        frame.markAlive(0,0);
        frame.markAlive(0,1);
        System.out.println(frame.toString());
        assertThat(frame.isAlive(0,0), is(true));
        Frame frame2 = frame.nextFrame();
        System.out.println(frame2.toString());
        assertThat(frame2.isAlive(0,0), is(false));
    }

    //Kui rakul on vähemalt kaks naabrit, siis ta elab.
    @Test
    public void cellWithTwoNeighbors_lives() {

        frame.markAlive(0,0);
        frame.markAlive(0,1);
        frame.markAlive(1,0);
        System.out.println(frame.toString());
        assertThat(frame.isAlive(0,0), is(true));
        Frame frame2 = frame.nextFrame();
        System.out.println(frame2.toString());
        assertThat(frame2.isAlive(0,0), is(true));
    }

    //Kui rakul on kolm naabrit, siis ta elab.
    @Test
    public void cellWithThreeNeighbors_lives() {

        frame.markAlive(0,0);
        frame.markAlive(0,1);
        frame.markAlive(1,0);
        frame.markAlive(1,1);
        System.out.println(frame.toString());
        assertThat(frame.isAlive(0,0), is(true));
        Frame frame2 = frame.nextFrame();
        System.out.println(frame2.toString());
        assertThat(frame2.isAlive(0,0), is(true));
    }


    //Kui rakul on üle kolme naabri, siis ta sureb (ülerahvastatus). Ülemine piirjuht.
    @Test
    public void cellWithFourNeighbors_dies() {
        frame.markAlive(1,1);
        frame.markAlive(0,0);
        frame.markAlive(0,1);
        frame.markAlive(1,0);
        frame.markAlive(1,2);
        System.out.println(frame.toString());
        assertThat(frame.isAlive(1,1), is(true));
        Frame frame2 = frame.nextFrame();
        System.out.println(frame2.toString());
        assertThat(frame2.isAlive(1,1), is(false));
    }

    //Kui surnud rakul (ehk rakk puudub) on täpselt kolm naabrit, siis ta elustub (paljunemine).
    @Test
    public void deadCellWithThreeNeighbors_comesToLife() {
        frame.markAlive(0,1);
        frame.markAlive(1,0);
        frame.markAlive(1,1);
        System.out.println(frame.toString());
        assertThat(frame.isAlive(0,0), is(false));
        Frame frame2 = frame.nextFrame();
        System.out.println(frame2.toString());
        assertThat(frame2.isAlive(0,0), is(true));
    }

    //Kui surnud rakul (ehk rakk puudub) on vähem kui kolm naabrit, siis ta ei elustu. Alumine piirjuht.
    @Test
    public void deadCellWithTwoNeighbors_notComesToLife() {
        frame.markAlive(0,1);
        frame.markAlive(1,0);
        System.out.println(frame.toString());
        assertThat(frame.isAlive(0,0), is(false));
        Frame frame2 = frame.nextFrame();
        System.out.println(frame2.toString());
        assertThat(frame2.isAlive(0,0), is(false));
    }

    //Kui surnud rakul (ehk rakk puudub) on rohkem kui kolm naabrit, siis ta ei elustu. Ülemine piirjuht.
    @Test
    public void deadCellWithFourNeighbors_notComesToLife() {
        frame.markAlive(0,0);
        frame.markAlive(0,1);
        frame.markAlive(1,0);
        frame.markAlive(1,2);
        System.out.println(frame.toString());
        assertThat(frame.isAlive(1,1), is(false));
        Frame frame2 = frame.nextFrame();
        System.out.println(frame2.toString());
        assertThat(frame2.isAlive(1,1), is(false));
    }


    //Lõplik kontroll. Paikne, järgmine kaader sama, kui esimene.
    @Test
    public void stationaryCase_nextFrameEqual() {

        frame.markAlive(1,2);
        frame.markAlive(1,3);
        frame.markAlive(2,1);
        frame.markAlive(2,4);
        frame.markAlive(3,2);
        frame.markAlive(3,3);

        System.out.println(frame.toString());
        Frame frame2 = frame.nextFrame();
        System.out.println(frame2.toString());
        assertThat(frame2.equals(frame),is(true));
    }

    //Lõplik kontroll. Pulsseeriv, järgmine kaader erinev ja ülejärgmine kaader sama, kui esimene.
    @Test
    public void pulserCase_nextNextFrameEqual() {

        frame.markAlive(1,1);
        frame.markAlive(1,2);
        frame.markAlive(2,1);
        frame.markAlive(3,4);
        frame.markAlive(4,3);
        frame.markAlive(4,4);

        System.out.println(frame.toString());
        Frame frame2 = frame.nextFrame();
        System.out.println(frame2.toString());
        assertThat(frame2.equals(frame),is(false));
        Frame frame3 = frame2.nextFrame();
        System.out.println(frame3.toString());
        assertThat(frame3.equals(frame),is(true));


    }

    //Lõplik kontroll. Liivuv, järgmine ja ülejärgmine kaader erinev, kui esimene.
    @Test
    public void moverCase_framesNotEqual() {


        frame.markAlive(0,1);
        frame.markAlive(1,2);
        frame.markAlive(1,3);
        frame.markAlive(2,1);
        frame.markAlive(2,2);


        System.out.println(frame.toString());
        Frame frame2 = frame.nextFrame();
        System.out.println(frame2.toString());
        assertThat(frame2.equals(frame),is(false));
        Frame frame3 = frame2.nextFrame();
        System.out.println(frame3.toString());
        assertThat(frame3.equals(frame),is(false));

    }


}