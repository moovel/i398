package gol;

import com.sun.org.apache.xpath.internal.SourceTree;

import java.util.Arrays;

public class Frame {
    private String [][] frame;
    private int frameWidth;
    private int frameHeight;
    public Frame(int width, int height) {
        frame = new String[height][width];
        frameWidth = width;
        frameHeight = height;
        for (int i = 0; i < frameHeight; i++) {
            for (int j = 0; j < frameWidth; j++) {
                frame[i][j] = "-";
            }
        }
    }

    @Override
    public String toString() {
        String framePrint = "";
        for (int i = 0; i < frameHeight; i++) {
            for (int j = 0; j < frameWidth; j++) {
                framePrint += frame[i][j];
            }
            framePrint += "\n";
        }

        return framePrint;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Frame)) {
            return false;
        }

        Frame newFrame = (Frame) obj;
        boolean control = Arrays.deepEquals(newFrame.frame, frame);
        return control;

    }

    public Integer getNeighbourCount(int x, int y) {
        int row1;
        int row2;
        int col1;
        int col2;
        int count = 0;
        if((x >= 0 && x <= frameHeight) && (y >= 0 && y <= frameWidth)) {
            if (x == 0) {
                row1 = 0;
            } else {
                row1 = x - 1;
            }

            if (x == frameHeight -1)
            {
                row2 = frameHeight -1 ;
            }else{
                row2 = x +1;
            }
            if (y == 0) {
                col1 = 0;
            } else {
                col1 = y - 1;
            }

            if (y == (frameWidth -1 ))
            {
                col2 = frameWidth - 1 ;
            }else{
                col2 = y + 1 ;
            }
            for (int i = row1; i <= row2; i++) {

                for (int j = col1; j <= col2; j++) {

                    if (isAlive(i, j) && !(i == x && j == y)) {
                        count++;
                    }
                }
            }

        }

        return count;
    }



    public Integer getNeighbourCount1(int y, int x) {
        int row1;
        int row2;
        int column1;
        int column2;
        int count = 0;
        if((y >= 0 && y <= frameHeight) && (x >= 0 && x <= frameWidth)) {
            if (y == 0) {
                row1 = 0;
            } else {
                row1 = y - 1;
            }

            if (y == frameHeight -1)
            {
                row2 = frameHeight -1 ;
            }else{
                row2 = y +1;
            }
            if (x == 0) {
                column1 = 0;
            } else {
                column1 = x - 1;
            }

            if (x == (frameWidth -1 ))
            {
                column2 = frameWidth - 1 ;
            }else{
                column2 = x + 1 ;
            }
            for (int i = row1; i <= row2; i++) {

                for (int j = column1; j <= column2; j++) {

                    if (isAlive(i, j) && !(i == y && j == x)) {
                        count++;
                    }
                }
            }

        }

        return count;
    }

    public boolean isAlive(int x, int y) {
         if(frame[x][y]== "X"){
             return true;
         }
         return false;
    }

    public void markAlive(int x, int y) {
        frame[x][y]= "X";
    }

    public Frame nextFrame() {
        Frame newFrame = new Frame(frameWidth,frameHeight);
        for (int i = 0; i < frameHeight; i++) {
            for (int j = 0; j < frameWidth; j++) {
                if(isAlive(i,j)){
                    if(getNeighbourCount(i,j) == 2 || getNeighbourCount(i,j) == 3){
                        newFrame.frame[i][j] = "X";
                    } else {
                        newFrame.frame[i][j] = "-";
                    }
                }else{
                    if(getNeighbourCount(i,j) == 3){
                        newFrame.frame[i][j] = "X";
                    }else {
                        newFrame.frame[i][j] = "-";
                    }
                }
            }
        }
        return newFrame;
    }

}