package stub;

import common.Money;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class TestableReport extends Report {

    private TestableBank bank = new TestableBank();

    @Override
    public Money getTotalIncomeBetween(Date startDate, Date endDate) {
        List<Money> incomes = Arrays.asList(new Money(1, "EUR"), new Money(15, "EEK"));

        Money total = new Money(0, "EUR");

        for (Money each : incomes) {
            total = total.plus(bank.convert(each, "EUR"));
        }

        return total;
    }

}
