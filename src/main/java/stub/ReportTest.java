package stub;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;

import common.Money;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class ReportTest {
    final Money money1 = new Money(1, "EUR");
    final Money money2 = new Money(15, "EEK");


    Bank bank = new Bank(){
        @Override
        public Money convert(Money money, String toCurrency) {
            if (!toCurrency.equals(money.getCurrency())) {
                return new Money(money.getAmount() / 15, "EUR");
            } else {
                return money;
            }
        }
    };


    Report report = new Report(){
        @Override
        public Money getTotalIncomeBetween(Date startDate, Date endDate) {
            List<Money> incomes = Arrays.asList(money1, money2);

            Money total = new Money(0, "EUR");

            for (Money each : incomes) {
                total = total.plus(bank.convert(each, "EUR"));
            }

            return total;
        }
    };


    @Test
    public void calculatesTotalFromAmounts() {

        report.setBank(new Bank());

        Money total = report.getTotalIncomeBetween(null, null);

        assertThat(total, is(new Money(2, "EUR")));
    }

}
