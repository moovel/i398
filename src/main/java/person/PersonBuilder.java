package person;

public class PersonBuilder {

    private String name = "Hunt";
    private Address address = new Address("Kase 9", "Metsa");
    private String mail = "hunt@kriimsilm.ee";
    private int age = 100;
    private String gender= "M";

    public PersonBuilder withAge(int age) {
        this.age = age;
        return  this;
    }

    public PersonBuilder withGender(String gender) {
        this.gender=gender;
        return this;
    }

    public PersonBuilder withAddress(Address address) {
        this.address=address;
        return this;
    }



    public Person build() {
        Person p = new Person(this.name,this.age,this.gender,this.address);
        return  p;
    }
}
