package person;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static java.util.Arrays.*;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.fail;

public class PersonUtilTest {

    private PersonUtil personUtil = new PersonUtil();

    @Test
    public void findsOldestPerson() {
        Person p1 = aPerson().withAge(32).build();
        Person p2 = aPerson().withAge(55).build();
        Person p3 = aPerson().withAge(21).build();

        assertThat(personUtil.getOldest(asList(p1, p2, p3)), is(p2));
    }

    @Test
    public void findsPersonsInLegalAge() {

        Person p1 = aPerson().withAge(10).build();
        Person p2 = aPerson().withAge(33).build();
        Person p3 = aPerson().withAge(1).build();

        assertThat(personUtil.getPersonsInLegalAge(asList(p1,p2,p3)), CoreMatchers.hasItem(p2));
    }

    @Test
    public void findsWomen() {

        Person p1 = aPerson().withGender(Person.GENDER_FEMALE).build();
        Person p2 = aPerson().withGender(Person.GENDER_FEMALE).build();
        Person p3 = aPerson().withGender(Person.GENDER_MALE).build();

        assertThat(personUtil.getWomen(asList(p1, p2, p3)), CoreMatchers.hasItems(p1, p2));
    }

    @Test
    public void findsPersonsLivingInSpecifiedTown() {
        Person p1 = aPerson().withAddress(new Address("Kase 9", "Metsa")).build();
        Person p2 = aPerson().withAddress(new Address("Männi 1", "Soo")).build();
        Person p3 = aPerson().withAddress(new Address("Kuuse 4", "Metsa")).build();

        assertThat(personUtil.getPersonsWhoLiveIn("Metsa", (asList(p1, p2, p3))), CoreMatchers.hasItems(p1, p3));

    }

    private PersonBuilder aPerson() {
        return new PersonBuilder();
    }
}
