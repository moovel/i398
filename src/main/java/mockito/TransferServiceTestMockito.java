package mockito;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import common.BankService;
import common.TransferService;
import common.Money;

@SuppressWarnings("unused")
public class TransferServiceTestMockito {

    BankService bankService = mock(BankService.class);
    TransferService transferService = new TransferService(bankService);

    @Test
    public void transferWithCurrencyConversion() {

        when(bankService.getAccountCurrency("E_123")).thenReturn("EUR");

        Money moneyEur = new Money(1,"EUR");
        Money moneySek = new Money(5, "SEK");
        when(bankService.getAccountCurrency("S_456")).thenReturn("SEK");
        when(bankService.hasSufficientFundsFor(moneyEur, "E_123")).thenReturn(true);

        // when(...
        // what needs to be taught can be found in common.TransferService.transfer() method.

        // transfer 1 EUR from account E_123 to account S_456
        // E_123 accounts currency is EUR
        // S_456 accounts currency is SEK
        when(bankService.convert(moneyEur, "EUR")).thenReturn(moneyEur);
        when(bankService.convert(moneyEur, "SEK")).thenReturn(moneySek);
        transferService.transfer(moneyEur, "E_123", "S_456");

       verify(bankService).withdraw(moneyEur, "E_123");
        verify(bankService).deposit(moneySek, "S_456");
        // verify(bankService).deposit(...
    }

    @Test
    public void transferWhenNotEnoughFunds() {
        when(bankService.getAccountCurrency("E_123")).thenReturn("EUR");
        when(bankService.convert(new Money(1,"EUR"), "EUR")).thenReturn(new Money(1,"EUR"));
        when(bankService.hasSufficientFundsFor(new Money(1,"EUR"), "E_123")).thenReturn(false);
        verify(bankService, never()).withdraw(anyMoney(), anyString());
    }

    private Money anyMoney() {
        return (Money) any();
    }

    private String anyAccount() {
        return anyString();
    }
}