package refactoring;

import java.util.*;

public class Refactoring {

    // 1a
    public int nextInvoiceNumber() {
        return ++invoiceNumber;
    }

    // 1b
    public void makeFilledOrdersList(List<Order> orderList) {
        for (Order order : orders) {
            if (order.isFilled()) {
                orderList.add(order);
            }
        }
    }

    // 2a
    public void printInvoiceRows(Long invoiceId) {
        List<InvoiceRow> invoiceRows = dao.getInvoiceRowsFor(invoiceId);

        printInvoiceRows(invoiceRows);

        // calculate invoice total
        ptintInvoiceTotal(invoiceRows);
    }

    private void ptintInvoiceTotal(List<InvoiceRow> invoiceRows) {
        double total = 0;
        for (InvoiceRow invoiceRow : invoiceRows) {
            total += invoiceRow.getAmount();
        }

        printValue(total);
    }

    // 2b
    public String getItemsAsHtml() {
        String result = "";
        result += "<ul>";
        for (String item : items) {
            result += createTagWithContent("li", item);
        }
        result += "</ul>";
        return result;
    }

    public String createTagWithContent(String tag, String item) {

        return "<"+tag+">" + item + "</"+tag+">";
    }

    // 3
    public boolean isSmallOrder() {
        return  (order.getTotal() > 100);
    }

    // 4
    public void printPrice() {
        System.out.println("Price not including VAT: " + getBasePrice());
        System.out.println("Price including VAT: " + getBasePrice() * VAT_TAX);
    }

    // 5
    public void calculatePayFor(Job job) {
        // on weekends at night
        final boolean onWeekendAtNightDay = (job.day == 6 || job.day == 7);
        final boolean onWeekendAtNightHour = (job.hour > 20 || job.hour < 7);

        if(onWeekendAtNightDay && onWeekendAtNightHour){

        }
    }

    // 6
    public boolean canAccessResource(SessionData sessionData) {
        // is admin and has preferred status

        return isAdminPreferredStatus(sessionData);
    }

    private boolean isAdminPreferredStatus(SessionData sessionData) {
        return (sessionData.getCurrentUserName().equals("Admin")
            || sessionData.getCurrentUserName().equals("Administrator"))
        && (sessionData.getStatus().equals("preferredStatusX")
            || sessionData.getStatus().equals("preferredStatusY"));
    }

    // 7
    public void drawLines() {
        Space space = new Space();
        space.drawLine( new LineCoordinates(12,3, 5), new LineCoordinates(2,4,6));
        space.drawLine( new LineCoordinates(2,4,6), new LineCoordinates(0,1,0));
    }

    // 8
    public int calculateWeeklyPayWithOvertime(int hoursWorked) {
        boolean overtime = true;
        int straightTime = Math.min(40, hoursWorked);
        int overTime = Math.max(0, hoursWorked - straightTime);
        int straightPay = straightTime * hourRate;
        double overtimeRate = 1.5 * hourRate;
        int overtimePay = (int) Math.round(overTime * overtimeRate);
        return straightPay + overtimePay;
    }

    public int calculateWeeklyPayWithoutOvertime(int hoursWorked) {
        boolean overtime = false;
        int straightTime = Math.min(40, hoursWorked);
        int overTime = Math.max(0, hoursWorked - straightTime);
        int straightPay = straightTime * hourRate;
        double overtimeRate = 1.0 * hourRate;
        int overtimePay = (int) Math.round(overTime * overtimeRate);
        return straightPay + overtimePay;
    }

    // //////////////////////////////////////////////////////////////////////////

    // Helper fields and methods.
    // They are here just to make the code compile

    private List<String> items = Arrays.asList("1", "2", "3", "4");
    private int hourRate = 5;
    int invoiceNumber = 0;
    private List<Order> orders = new ArrayList<>();
    private Order order = new Order();
    private Dao dao = new SampleDao();
    private double price = 0;
    private static final double VAT_TAX = 1.2;

    void justACaller() {
        nextInvoiceNumber();
        makeFilledOrdersList(null);
    }

    private void printValue(double total) {
    }

    private void printInvoiceRows(List<InvoiceRow> invoiceRows) {
    }

    class Space {
        public void drawLine(LineCoordinates lineStartCordinates, LineCoordinates lineEndCordinates) {
        }

    }

    interface Dao {
        List<InvoiceRow> getInvoiceRowsFor(Long invoiceId);
    }

    class SampleDao implements Dao {
        @Override
        public List<InvoiceRow> getInvoiceRowsFor(Long invoiceId) {
            return null;
        }
    }

    class Order {
        public boolean isFilled() {
            return false;
        }

        public double getTotal() {
            return 0;
        }
    }

    class InvoiceRow {
        public double getAmount() {
            return 0;
        }
    }

    class Job {
        public int hour;
        public int day;
    }

    private double getBasePrice() {
        return price;
    }

    private class SessionData {

        public String getCurrentUserName() {
            return null;
        }

        public String getStatus() {
            return null;
        }

    }

}
